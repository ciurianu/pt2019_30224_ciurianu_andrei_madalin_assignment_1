package View;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import FindPattern.FindPattern;
import Polinom.Polinom;

public class MainView extends JFrame{

	private JLabel l1;
	private JLabel l2;
	private JLabel l3;
	private JPanel pVertL;
	private JPanel pVertR;
	private JPanel pVertM;
	private JPanel pHoriz;
	private JButton Adunare;
	private JButton Scadere;
	private JButton Div;
	private JButton Mul;
	private JButton Derivare;
	private JButton Integrare;
	private JButton Clear;
	private JTextField P1;
	private JTextField P2;
	private JTextField Rez;
	
	public MainView() {
		
		setTitle("Polinom");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(new Dimension( 400, 400));
		setResizable(false);
		setLocationRelativeTo(null);
		pVertL = new JPanel();
		pVertR = new JPanel();
		pVertM = new JPanel();
		pHoriz = new JPanel();
		pHoriz.setLayout(new BoxLayout(pHoriz,BoxLayout.X_AXIS));
		pVertL.setLayout(new BoxLayout(pVertL,BoxLayout.Y_AXIS));
		pVertR.setLayout(new BoxLayout(pVertR,BoxLayout.Y_AXIS));
		pHoriz.add( pVertL );
		pHoriz.add( Box.createRigidArea(new Dimension(20,0)) );
		pHoriz.add(pVertM);
		pHoriz.add( Box.createRigidArea(new	Dimension(80,0)) );
		pHoriz.add( pVertR );
		
		
		addComponenets();
		
	}
	
	private void addComponenets() {
		
		Adunare = new JButton("Adunare");
		Scadere = new JButton("Scadere");
		Div = new JButton("Impartire");
		Mul = new JButton("Inmultire");
		Derivare = new JButton("Derivare");
		Integrare = new JButton("Integrare");
		Clear = new JButton("Clear");
		
		pVertR.add(Adunare);	
		pVertR.add(Scadere);	
		pVertR.add(Div);	
		pVertR.add(Mul);	
		pVertR.add(Derivare);	
		pVertR.add(Integrare);	
		pVertR.add(Clear);
		
		pVertR.add(Box.createRigidArea(new Dimension(0,25)));
		
		
		l1 = new JLabel("Polinom 1:", JLabel.RIGHT);
		pVertL.add( l1 );
		pVertL.add(Box.createRigidArea(new Dimension(0,20)));
		l2 = new JLabel("Polinom 2:", JLabel.RIGHT);
		pVertL.add( l2 );
		pVertL.add(Box.createRigidArea(new Dimension(0,20)));
		l3 = new JLabel("Rezultat:", JLabel.RIGHT);
		pVertL.add( l3 );
		
	    P1 = new JTextField(10);	
	    P2 = new JTextField(10);
		Rez = new JTextField(10);
		Rez.setEditable(false);
		pVertM.add(Box.createRigidArea(new Dimension(100,125)));
		pVertM.add(P1);
		pVertM.add(Box.createRigidArea(new Dimension(0,25)));
		pVertM.add(P2);
		pVertM.add(Box.createRigidArea(new Dimension(0,35)));
		pVertM.add(Rez);
		pVertM.add(Box.createRigidArea(new Dimension(0,25)));
		
		
		
		
		add(pHoriz);
		
	}
	
	public void AdunareButton(final ActionListener actionListener) {
		Adunare.addActionListener(actionListener);
	}
	
	public void ScadereButton(final ActionListener actionListener) {
		Scadere.addActionListener(actionListener);
	}
	
	public void InmultireButton(final ActionListener actionListener) {
		Mul.addActionListener(actionListener);
	}
	
	public void ImpartireButton(final ActionListener actionListener) {
		Div.addActionListener(actionListener);
	}
	
	public void DerivareButton(final ActionListener actionListener) {
		Derivare.addActionListener(actionListener);
	}
	
	public void IntegrareButton(final ActionListener actionListener) {
		Integrare.addActionListener(actionListener);
	}
	public void ClearButton(final ActionListener actionListener) {
		Clear.addActionListener(actionListener);
	}
	
	
	public Polinom getFirstPolinom() {
		String p1 = P1.getText();
		FindPattern pattern = new FindPattern(p1);
		int[] ints = pattern.getNumbers();
		Polinom temp = new Polinom(ints);
		
		return temp;
		
	}
	public Polinom getSecondPolinom() {
		String p2 = P2.getText();
		FindPattern pattern = new FindPattern(p2);
		int[] ints = pattern.getNumbers();
		Polinom temp = new Polinom(ints);
		
		return temp;
		
	}
	public String getFirstText() {
		return P1.getText();
	}
	
	public String getSecondText() {
		return P2.getText();
	}
	
	public void setRez(String rez) {
		Rez.setText(rez);
	}
	public void Clear() {

		Rez.setText("");
	}
	public void ClearText() {
		P1.setText("");
		P2.setText("");
		
	}
	public void setP1() {
		P1.setText("3x^3-1x^2+4x^0");
	}
	public void setP2() {
		
		P2.setText("-2x^3+4x^1");
	}
}
