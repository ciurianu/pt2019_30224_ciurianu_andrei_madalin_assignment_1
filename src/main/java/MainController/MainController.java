package MainController;

import javax.swing.JOptionPane;

import Evaluate.Calculator;
import View.MainView;

public class MainController {

	private MainView mainView;
	
	public MainController() {
		mainView = new MainView();
		mainView.setVisible(true);
		mainView.setP1();
		mainView.setP2();
		
		
		
		initializeActionListeners();
	}
	
	private void initializeActionListeners() {
		
		
		initializeAdunare();
		initializeScadere();
		initializeImpartire();
		initializeInmultire();
		initializeDerivare();
		initializeIntegrare();
		intitializeClear();
		
	}
	
	private void initializeAdunare() {
		mainView.AdunareButton(e->{
			
			mainView.Clear();
			if(mainView.getFirstText().equals("") || mainView.getSecondText().equals("")) {
				JOptionPane.showMessageDialog(null, "Nu ai introdus unul din polinoame", "Mesaj", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				mainView.setRez(new Calculator().plus(mainView.getFirstPolinom(),mainView.getSecondPolinom()).toString());
			}
		});
	}
	
	private void initializeScadere() {
		
		mainView.ScadereButton(e->{
			mainView.Clear();
			if(mainView.getFirstText().equals("") || mainView.getSecondText().equals("")) {
				
				JOptionPane.showMessageDialog(null, "Nu ai introdus unul din polinoame", "Mesaj", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				mainView.setRez(new Calculator().minus(mainView.getFirstPolinom(), mainView.getSecondPolinom()).toString());
			}
		});
		
		
	}
	
	private void initializeImpartire() {
		mainView.ImpartireButton(e->{
			mainView.Clear();
			if(mainView.getFirstText().equals("") || mainView.getSecondText().equals("")) {
				
				JOptionPane.showMessageDialog(null, "Nu ai introdus unul din polinoame", "Mesaj", JOptionPane.INFORMATION_MESSAGE);
			}
		});
	}
	
	private void initializeInmultire() {
		mainView.InmultireButton(e->{
			mainView.Clear();
			if(mainView.getFirstText().equals("") || mainView.getSecondText().equals("")) {
				
				JOptionPane.showMessageDialog(null, "Nu ai introdus unul din polinoame", "Mesaj", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				mainView.setRez(new Calculator().mul(mainView.getFirstPolinom(), mainView.getSecondPolinom()).toString());
			}
		});
	}
	
	private void initializeDerivare() {
		mainView.DerivareButton(e->{
			mainView.Clear();
			if(mainView.getFirstText().equals("") || mainView.getSecondText().equals("")) {
				
				JOptionPane.showMessageDialog(null, "Nu ai introdus unul din polinoame", "Mesaj", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				mainView.setRez(new Calculator().derivare(mainView.getFirstPolinom()).toString());
			}
		});
	}
	
	private void initializeIntegrare() {
		mainView.IntegrareButton(e->{
			mainView.Clear();
			if(mainView.getFirstText().equals("") || mainView.getSecondText().equals("")) {
				
				JOptionPane.showMessageDialog(null, "Nu ai introdus unul din polinoame", "Mesaj", JOptionPane.INFORMATION_MESSAGE);
			}
			else {
				mainView.setRez(new Calculator().integrare(mainView.getFirstPolinom()).toString());
			}
		});
	}
	
	private void intitializeClear() {
		mainView.ClearButton(e->{
			mainView.ClearText();
		});
	
	}
}
