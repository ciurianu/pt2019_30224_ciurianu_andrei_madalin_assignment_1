package Test;

import Evaluate.Calculator;
import Polinom.Polinom;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestApp {
	
	Calculator calc = new Calculator();
	int[] vals1 = {3,3,-2,1,4,0};
	int[] vals2 = {2,4,-1,3,-4,2,1,1};
	
	Polinom p1 = new Polinom(vals1); //3x^3-2x+4
	Polinom p2 = new Polinom(vals2); //2x^4-x^3-4x^2+x
	private String string;

	@Test
	public void testAdunare() {
		
		Polinom rez = new Polinom();
		rez = calc.plus(p1, p2);
		string = "2.0x^4 + 2.0x^3 -4.0x^2 -1.0x^1 + 4.0x^0";
		assertTrue(rez.toString().equals(string));
	}
	
	@Test
	public void testScadere() {
		Polinom rez = new Polinom();
		rez = calc.minus(p1, p2);
		string = "-2.0x^4 + 4.0x^3 + 4.0x^2 -3.0x^1 + 4.0x^0";
		assertTrue(rez.toString().equals(string));
	}
	@Test
	public void testInmultire() {
		Polinom rez = new Polinom();
		rez = calc.mul(p1, p2);
		string = "6.0x^7 -3.0x^6 -16.0x^5 + 13.0x^4 + 4.0x^3 -18.0x^2 + 4.0x^1";
		assertTrue(rez.toString().equals(string));
	}
	@Test
	public void testIntegrare() {
		Polinom rez = new Polinom();
		rez = calc.integrare(p1);
		string = "0.75x^4 -1.0x^2 + 4.0x^1";
		assertTrue(rez.toString().equals(string));
	}
	@Test
	public void testDerivare() {
		Polinom rez = new Polinom();
		rez = calc.derivare(p1);
		string = "9.0x^2 -2.0x^0";
		assertTrue(rez.toString().equals(string));
	}
	
	
}
