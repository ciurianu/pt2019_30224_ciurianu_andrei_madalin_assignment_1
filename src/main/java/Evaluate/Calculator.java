package Evaluate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import Polinom.Monom;
import Polinom.Polinom;

public class Calculator {

	public Polinom rest= new Polinom();
	
	
	public boolean isMonom(Monom a, Polinom p) {
		
		for(Monom i : p.getPolinom()) {
			if(i.getDeg() == a.getDeg()) {
				return true;
			}
		}
		return false;
	}
	

	public Polinom plus(Polinom a, Polinom b) {
		Polinom temp = new Polinom();
		for(Monom i : a.getPolinom()) {
			for(Monom j : b.getPolinom()) {
				if(i.getDeg() == j.getDeg()) {
					temp.addMonom(new Monom(i.getCoef()+j.getCoef(),i.getDeg()));
				}
			}
			if(isMonom(i,temp) == false) {
				temp.addMonom(new Monom(i.getCoef(),i.getDeg()));
			}	
		}
		for (Monom i : b.getPolinom())
			if (isMonom(i, temp) == false)
				temp.addMonom(new Monom(i.getCoef(), i.getDeg()));
		temp.sortPolinom();
		return temp;
	}
	
	public Polinom minus(Polinom a, Polinom b) {
		Polinom temp = new Polinom();
		for(Monom i : a.getPolinom()) {
			for(Monom j : b.getPolinom()) {
				if(i.getDeg() == j.getDeg()) {
					temp.addMonom(new Monom(i.getCoef()-j.getCoef(),i.getDeg()));
				}
			}
			if(isMonom(i,temp) == false) {
				temp.addMonom(new Monom(i.getCoef(),i.getDeg()));
			}	
		}
		for (Monom i : b.getPolinom())
			if (isMonom(i, temp) == false)
				temp.addMonom(new Monom(-i.getCoef(), i.getDeg()));
		temp.sortPolinom();
		return temp;
		
	}
	
	public Polinom mul(Polinom a, Polinom b) {
		Polinom temp = new Polinom();
		
		for(Monom i : a.getPolinom()) {
			Polinom temp2 = new Polinom();
			for(Monom j : b.getPolinom()) {
				temp2.addMonom(new Monom(i.getCoef()*j.getCoef(),i.getDeg()+j.getDeg()));
			}
			temp = plus(temp,temp2);
		}
		
		temp.sortPolinom();
		return temp;
	}
	
	
	public Polinom derivare(Polinom a) {
		Polinom temp = new Polinom();
		
		for(Monom i : a.getPolinom()) {
			if(i.getDeg() != 0) {
				temp.addMonom(new Monom(i.getCoef() * i.getDeg(),i.getDeg()-1));
			}
		}
		temp.sortPolinom();
		return temp;
		
	}
	
	public Polinom integrare(Polinom a) {
		Polinom temp = new Polinom();
		for(Monom i : a.getPolinom()) {
			if(i.getDeg() != 0) {
				temp.addMonom(new Monom(i.getCoef()/(i.getDeg()+1),i.getDeg()+1));
			}
			else {
				temp.addMonom(new Monom(i.getCoef(),i.getDeg()+1));
			}
		}
		temp.sortPolinom();
		return temp;
	}
	
			

}
