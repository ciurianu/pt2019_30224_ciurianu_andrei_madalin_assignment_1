package Polinom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Polinom {
	
	private List<Monom> terms;
	private Monom monom;
	
	public Polinom() {
		
		terms = new ArrayList<Monom>();
	}
	
	public Polinom(int [] vals) {
		
		terms = new ArrayList<Monom>();
		int n = vals.length;
		
		for(int i = 0; i < n ; i += 2) {
			terms.add(new Monom(vals[i],vals[i+1]));
		}
		
	}
	
	
	public Monom getMonom() {
		return monom;
	}

	public void setMonom(Monom monom) {
		this.monom = monom;
	}
	public void addMonom(Monom m) {
		
		terms.add(m);
	}

	public List<Monom> getPolinom() {
		return terms;
	}
	public void setPolinom(List<Monom> polinom) {
		this.terms = polinom;
	}
	
	public void sortPolinom() {
		Collections.sort(terms);
	}

	@Override
	public String toString() {
		
		String x= new String();
		for(Monom i : terms) {
			
			if(i==terms.get(0))
			{
					x+=i.toString();
		
			}
			else if(i!=terms.get(0)) {
				if(i.getCoef()<0) {
					x+=" "+i.toString();
				}
				else if(i.getCoef()>0) {
					x+=" + "+i.toString();
				}
			}
		}
		
		return x;
	}
	
	
	
}
	

