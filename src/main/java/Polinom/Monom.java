package Polinom;

public class Monom implements Comparable<Monom> {
	
	private float coef;
	private int deg;
	
	public Monom() {
		
		this.setCoef(0);
		this.setDeg(0);
	}
	
	public Monom(float coef,int deg) {
		
		this.setCoef(coef);
		this.setDeg(deg);
	}

	public float getCoef() {
		return coef;
	}

	public void setCoef(float coef) {
		this.coef = coef;
	}

	public int getDeg() {
		return deg;
	}

	public void setDeg(int deg) {
		this.deg = deg;
	}
	

	@Override
	public String toString() {
		
		return coef +"x^" + deg;
	}

	@Override
	public int compareTo(Monom m) {
		
		return m.deg - this.deg;
		//return m.deg - this.deg;
	}
	
	
}
