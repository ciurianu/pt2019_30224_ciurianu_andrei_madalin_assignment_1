package FindPattern;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FindPattern {
	
	private Pattern p;
	private Matcher m;
	private String str;
	
	public FindPattern(String str) {
		
		this.str = str;
		
	}
	
	public int[] getNumbers() {
		
		p = Pattern.compile("-?\\\\d+");
        m = p.matcher(str);
        
        int[] ints = Arrays.stream(str.replaceAll("-", " -").split("[^-\\d]+"))
                .filter(s -> !s.matches("-?"))
                .mapToInt(Integer::parseInt).toArray();
        
        
        return ints;
	}
	
	
	
}
